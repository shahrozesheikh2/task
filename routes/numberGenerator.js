var express = require('express');
var router = express.Router();
var userModel=require('../models/user.model')
var roleModel=require('../models/role.model')
var numberGenatorModel = require('../models/generatedDetails.model')
const config = require('../config.json');
const { verifyJwt } = require("../common/authentication");
console.log("verifyJwt",verifyJwt)


const http = require('http').createServer(router);
const io = require('socket.io')(http, {
    cors: {
      origins: ['http://localhost:4200']
    }
  });



router.get('/', (req, res) => {
  res.send('<h1>Hey Socket.io</h1>');
});
io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
      console.log('user disconnected');
    });
  });


router.post('/genratedNumber', verifyJwt ,async function(req, res, next) {

    try{
  
    const obj = req.body;

    // console.log("obj",obj)

    const generateNumberReq = new numberGenatorModel({
        gameName:obj.gameName,
        gameTypeId:obj.gameTypeId,
        photobanner:obj.photobanner,
        gameCategoryId:obj.gameCategoryId,
        generateNumber:obj.generateNumber,
        startingNumber:obj.startingNumber,
        endingNumber:obj.endingNumber,
        removeNumbers:obj.removeNumbers,
        DrawType:obj.DrawType,
        ResultPosting:obj.ResultPosting,
        GameStartTime:obj.GameStartTime,
        GameStartEnd:obj.GameStartEnd,
        DrawTime:obj.DrawTime,
        Repeat:obj.Repeat,
        TotalPlayer:obj.TotalPlayer,
        GateTime:obj.GateTime,
        EntryPoint:obj.EntryPoint,
        Rewards:obj.Rewards,
        RewardType:obj.RewardType,
        SharingPercentage:obj.SharingPercentage,
        ShareWith:obj.ShareWith,
        Status:obj.Status,
    });

    // console.log("genrateNumberReq",genrateNumberReq)
    
    const numbers = await generateNumberList(obj.startingNumber,obj.endingNumber,obj.removeNumbers,obj.generateNumber)
  
    console.log("numbers",numbers)

    console.log("numberGenatorModel",numberGenatorModel)

    var result = await generateNumberReq.save();

    console.log("result",result)

    const respone = {result,numbers}
  
    res.status(200).send({ message: "all", respone, error: {} })
  
    }catch(err){
         return err
    }
  
  });

  async function generateNumberList(lowerLimit, upperLimit, numberstoExclude, totalNumbersToGenerate){
    let numberList=[];
    for (let i = 0; i < totalNumbersToGenerate; i++) {
        let number=genrateRandomNumber(lowerLimit, upperLimit, numberstoExclude);
        while(numberList.includes(number)){
            number = genrateRandomNumber(lowerLimit, upperLimit, numberstoExclude);
        }
        numberList.push(number);
    }
    return numberList;
}

 function genrateRandomNumber(lowerLimit, upperLimit, numberstoExclude){
    let randomNumber = Math.floor((Math.random() * upperLimit) + lowerLimit);
    return numberstoExclude.includes(randomNumber)?genrateRandomNumber(lowerLimit, upperLimit, numberstoExclude):randomNumber;
}


module.exports = router;