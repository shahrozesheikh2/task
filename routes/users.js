var express = require('express');
var router = express.Router();
var userModel=require('../models/user.model')
var roleModel=require('../models/role.model')
const config = require('../config.json');



const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.post('/signUp', async function(req, res, next) {

  try{

    const obj = req.body;

    const query  = userModel.where({ email: obj.email });

    const already = await  query.findOne()

    console.log("already",already);

    if (already) {
      res.send("Email already exists");
  }
  const saltRound = 10;
  const hash = bcrypt.hashSync(obj.password, saltRound);

  const user = new userModel({
    firstName: obj.firstName,
    lastName:obj.lastLame,
    email: obj.email,
    password: hash,
    role_id:obj.roleId
  });

  var result = await user.save();

  res.status(200).send({ message: "Sucessfully", result, error: {} })

  }catch(err){
       return err
  }

});
router.post('/signIn', async function(req, res, next) {

  try{

    const obj = req.body;

    const query  = userModel.where({ email: obj.email });

    const existUser = await  query.findOne()

    console.log("exist",existUser);

    if (!existUser) {
      res.send("User Not exists");
    }
    const hash =  bcrypt.compareSync(obj.password, existUser.password)
    console.log("hash",hash);
        if (hash == false) {
            throw new Error("Invalid Credential")
        }
     let token = jwt.sign({
            id: existUser.id,
            roleId: existUser.role_id,
        }, config.jwt.secret, { expiresIn: '12D' })
      
      const response= { userId: existUser.id, roleId: existUser.role_id, token, user: existUser};

    res.status(200).send({ message: "Sucessfully", response, error: {} })

  }catch(err){
       return err
  }

})
router.get('/roles', async function(req, res, next) {

    const roles = await  roleModel.find()

    console.log("roles",roles);

    res.status(200).send({ message: "Sucessfully", roles, error: {} })


  res.send('respond with a resource');
});



module.exports = router;
