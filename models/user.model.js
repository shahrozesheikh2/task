const mongoose = require("mongoose");

const users = mongoose.Schema({
    email:String,
    password:String,
    firstName:String,
    lastLame:String,
    role_id: { type: mongoose.Schema.Types.ObjectId, ref: 'roles' }
});

module.exports = mongoose.model("users", users);