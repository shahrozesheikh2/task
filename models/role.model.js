const mongoose = require("mongoose");

const roles = mongoose.Schema({
   name:String
});

module.exports = mongoose.model("roles", roles);