const mongoose = require("mongoose");

const generatedDetails = mongoose.Schema({
    gameName:String,
    gameTypeId:Number,
    photobanner:String,
    gameCategoryId:Number,
    generateNumber:Number,
    startingNumber:Number,
    endingNumber:Number,
    removeNumbers:[Number],
    DrawType:String,
    ResultPosting:String,
    GameStartTime:Date,
    GameStartEnd:Date,
    DrawTime:Date,
    Repeat:String,
    TotalPlayer:Number,
    GateTime:Date,
    EntryPoint:Number,
    Rewards:Number,
    RewardType:String,
    SharingPercentage:Number,
    ShareWith:Number,
    Status:String,

});

module.exports = mongoose.model("generatedDetails", generatedDetails);